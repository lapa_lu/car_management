{
    'name': 'Car management',
    'version': '10.0.0.1',
    'category': 'Others',
    'description': """""",
    'author': 'LogicaSoft SPRL',
    'depends': ['base', 'sale', 'website'],
    'data': [
        'views/rent.xml',
        'views/car.xml',
        'views/car_type.xml',
        'views/res_partner.xml',
        'wizards/car_returning.xml',
        'security/car_management_security.xml',
        'security/ir.model.access.csv',
        'views/car_report.xml',
        'reports/car_report_view.xml',
        'views/report_car_history.xml',
        'views/website_rent_template.xml',
        'data/website_menu.xml'
    ],
    'css': [],
    'test': [],
    'installable': True
}