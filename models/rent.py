from odoo import models, fields, api, exceptions
from datetime import datetime, timedelta, date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT


class Rent(models.Model):
    _name = 'rent'
    _description = 'renting of a car'
    _order = 'start_date desc'
    # _rec_name = 'car_id'

    start_date = fields.Date('Rent start on', help='start of the rent', required=True, default=date.today())
    end_date = fields.Date('Rent end on', help='end of the rent', required=True)
    renter_id = fields.Many2one('res.partner', 'Renter', help='guy who rent the car', required=True)
    car_id = fields.Many2one('car', 'Car', help='car which was rented by a guy', required=True)
    tank_refilled = fields.Boolean('Tank refilled', default=False)
    remark = fields.Text('Remark', help='some remarks on the rent')
    door_number = fields.Integer('Door number', related='car_id.door')
    name = fields.Char('Name')
    duration_in_days = fields.Integer('Duration in days', compute='get_duration')
    sale_order_id = fields.Many2one('sale.order', 'Order id')

    company = fields.Many2one('res.company', 'Company', default=lambda self: self.env.ref('base.main_company').id)
    currency_id = fields.Many2one('res.currency', related='company.currency_id')

    @api.onchange('start_date')
    def _onchange_start_date(self):
        self.end_date = datetime.strptime(self.start_date, DATE_FORMAT) + timedelta(days=7)

    @api.model
    def create(self, vals):
        rent_id = super(Rent, self).create(vals)
        sale_id = self.env['sale.order'].create({'partner_id': rent_id.renter_id.id,
                                                 'date_order': rent_id.start_date,
                                                 'state': 'sale'})
        sale_id.message_subscribe(partner_ids=rent_id.renter_id.ids)
        rent_id.sale_order_id = sale_id
        sol_id = self.env['sale.order.line'].create({
            'product_id': rent_id.car_id.product_id.id,
            'uom': rent_id.car_id.product_id.uom_id.id,
            'order_id': sale_id.id,
            'product_uom_qty': rent_id.duration_in_days
        })
        sol_id.product_id_change()
        return rent_id

    @api.multi
    @api.depends('car_id', 'renter_id')
    def name_get(self):
        super(Rent, self).name_get()
        res = []
        for field in self:
            res.append((field.id, ('%s - %s' % (field.renter_id.name, field.car_id.name))))
        return res

    @api.multi
    def get_duration(self):
        for record in self:
            start_date = datetime.strptime(record.start_date, DATE_FORMAT)
            end_date = datetime.strptime(record.end_date, DATE_FORMAT)
            record.duration_in_days = (end_date - start_date).days + 1

    @api.constrains('start_date', 'end_date')
    def start_date_check(self):
        if self.start_date > self.end_date:
            raise exceptions.ValidationError('Start date must be anterior to the end date')

    @api.constrains('car_id', 'start_date', 'end_date')
    def check_car_availability(self):
        rent_ids = self.env['rent'].search([('car_id', '=', self.car_id.id)])
        rent_ids = rent_ids.filtered(lambda r: r.start_date < self.start_date < r.end_date or
                                               r.start_date < self.end_date < r.end_date)
        if rent_ids:
            raise exceptions.ValidationError('Car not available.')
