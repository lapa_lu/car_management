from odoo import models, fields


class CarType(models.Model):
    _name = 'car.type'
    _description = 'type of the vehicle'

    name = fields.Char('Type', required=True)
