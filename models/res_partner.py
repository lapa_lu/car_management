from odoo import models, fields


class ResPartner(models.Model):
    _inherit = 'res.partner'

    car_ids = fields.One2many('rent', 'renter_id', 'Cars rented')
