from odoo import models, fields


class Car(models.Model):
    _name = 'car'
    _description = 'information on a vehicle'
    _order = 'license_plate asc'

    name = fields.Char('Name', required=True)
    license_plate = fields.Char('License Plate', required=True, help='License plate number of the vehicle')
    door = fields.Integer('Number of doors', help='number of doors of the vehicle', default=5, required=True)
    color = fields.Char('Color', required=True)
    image = fields.Binary('Image')
    website_published = fields.Boolean('Published', default=False)

    type_ids = fields.Many2many('car.type', string='Tags')
    rent_ids = fields.One2many('rent', 'car_id', help='rent made with this car')
    product_id = fields.Many2one('product.product')

    def update_publish_status(self):
        self.website_published = not self.website_published
