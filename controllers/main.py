from odoo import http
from odoo.http import request
from odoo.addons.website_portal.controllers.main import website_account


class WebsiteRentController(http.Controller):
    def _prepare_portal_layout_values(self):
        """ prepare the values to render portal layout """
        partner = request.env.user.partner_id
        # get customer sales rep
        sales_rep = partner.user_id
        return {
            'sales_rep': sales_rep,
            'company': request.website.company_id,
            'user': request.env.user
        }

    @http.route('/rent', type="http", auth='public', website=True)
    def rents(self):
        result = {'cars': request.env['car'].search([('website_published', '=', True)])}
        return request.render('car_management.rent_creation', result)

    @http.route(['/rent/new'], type='http', auth="user", methods=['POST'], website=True)
    def create_rent(self, **post):
        user_id = request.env.context.get('uid')
        user = request.env['res.users'].search([('id', '=', user_id)])
        request.env['rent'].sudo().create({'start_date': post['start_date'],
                                           'end_date': post['end_date'],
                                           'car_id': post['car_id'],
                                           'renter_id': user.partner_id.id})

    @http.route(['/my/rents'], type='http', auth='user', website=True)
    def portal_my_rents(self):
        values = self._prepare_portal_layout_values()
        rents = request.env['rent'].search([])
        values.update({
            'rents': rents
        })
        return request.render('car_management.portal_my_rents', values)


class website_account(website_account):

    @http.route()
    def account(self, **kw):
        response = super(website_account, self).account(**kw)
        response.qcontext.update({
            'rent_count': request.env['rent'].search_count([])
        })
        return response
