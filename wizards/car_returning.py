from odoo import api, fields, models


class CarReturning(models.TransientModel):
    _name = 'car.returning'
    _description = 'Car returning'

    rent_id = fields.Many2one('rent', 'Rent', help='rent to modify', required=True)
    new_end_date = fields.Date('NewEndDate', help='new end date', required=True)

    @api.multi
    def change_end_date(self):
        self.sudo().rent_id.end_date = self.new_end_date
